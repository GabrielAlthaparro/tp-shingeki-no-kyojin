package juego;

import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Suero {
  private double x, y;
  private int width, height;
  private double escala = 0.1;
  private Image img;
  Suero(Juego juego, Obstaculo[] obstaculos, Personaje mikasa){
    img = Herramientas.cargarImagen("suero.png");
    width = (int) (img.getWidth(null) * escala);
		height = (int) (img.getHeight(null) * escala);
    double[] coordenadas = randomXY(juego, mikasa, obstaculos);
    x = coordenadas[0];
    y = coordenadas[1];
  }

  public void dibujarse(Entorno entorno){
    entorno.dibujarImagen(img, x, y, 0, escala);
  }

  private double[] randomXY(Juego juego, Personaje mikasa, Obstaculo[] obstaculos) {
    double[] coordenadas = new double[2];
    double x, y;
    Random random = new Random();
    int widthPantalla = juego.getWidthPantalla();
    int heightPantalla = juego.getHeightPantalla();
    int mikasaWidthTitan = mikasa.getWidthTitan();
    int mikasaHeightTitan = mikasa.getHeightTitan();
    x = random.nextInt(widthPantalla - mikasaWidthTitan * 2) + mikasaWidthTitan;
    y = random.nextInt(heightPantalla - mikasaHeightTitan * 2) + mikasaHeightTitan;
    while (mikasa.estaCerca(x, y, width, height) || Obstaculo.rectanguloSobreObstaculo(obstaculos, x, y, mikasaWidthTitan * 2, mikasaHeightTitan * 2)){ // paso width y heigth de mikasa para dejar espacio para transformar
      x = random.nextInt(widthPantalla - mikasaWidthTitan * 2) + mikasaWidthTitan;
      y = random.nextInt(heightPantalla - mikasaHeightTitan * 2) + mikasaHeightTitan;
    }
    coordenadas[0] = x;
    coordenadas[1] = y;
    return coordenadas;
  }

  public boolean tocandoMikasa(Personaje mikasa){
    return mikasa.seSuperpone(x, y, width, height) ? true : false;
  }

  // getters
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}

}
