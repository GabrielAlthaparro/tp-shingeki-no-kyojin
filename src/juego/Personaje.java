package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Personaje {
	private double x, y;
	private int width, height;
	private double angulo = 0;
	private int velocidad;
	private double escala = 0.65;
	private double widthColision, heightColision;
	private Point arriba, abajo, izq, der;
	private Point diagonalArribaIzq;
	private Point diagonalArribaDer;
	private Point diagonalAbajoIzq;
	private Point diagonalAbajoDer;
	private Image imgIzq, imgDer;
	private Image imgActual;
	
	private boolean transformada = false;
	private int widthTitan, heightTitan;
	private double widthColisionTitan, heightColisionTitan;
	private Image imgTitanIzq, imgTitanDer;

	private Image flecha;
	private double xDistFlecha;
	private double yDistFlecha;

	Personaje(int x, int y, int velocidad) {
		this.x = x / 2;
		this.y = y / 2;
		this.velocidad = velocidad;
		imgIzq = Herramientas.cargarImagen("mikasa-izquierda.png");
		imgDer = Herramientas.cargarImagen("mikasa-derecha.png");
		imgActual = imgDer;
		width = (int) (imgDer.getWidth(null) * escala);
		height = (int) (imgDer.getHeight(null) * escala);

		imgTitanIzq = Herramientas.cargarImagen("kyojina-izquierda.png");
		imgTitanDer = Herramientas.cargarImagen("kyojina-derecha.png");
		widthTitan = (int) (imgTitanDer.getWidth(null) * escala);
		heightTitan = (int) (imgTitanDer.getHeight(null) * escala);

		flecha = Herramientas.cargarImagen("flecha.png");
		xDistFlecha = width / 2 * 1.25;
		yDistFlecha = height / 2 * 1.25;

		widthColision = width * 0.35;
		heightColision = height * 0.4;
		widthColisionTitan = widthTitan * 0.35;
		heightColisionTitan = heightTitan * 0.45;
		izq = new Point(this.x - widthColision, this.y);
		der = new Point(this.x + widthColision, this.y);
		arriba = new Point(this.x, this.y - heightColision);
		abajo = new Point(this.x, this.y + heightColision);
		diagonalArribaIzq = new Point(this.x - widthColision, this.y - heightColision);
		diagonalArribaDer = new Point(this.x + widthColision, this.y - heightColision);
		diagonalAbajoIzq = new Point(this.x - widthColision, this.y + heightColision);
		diagonalAbajoDer = new Point(this.x + widthColision, this.y + heightColision);
	}

	public void dibujarse(Entorno entorno) {
		entorno.dibujarImagen(imgActual, x, y, 0, escala); // dibujar a mikasa
		entorno.dibujarImagen(flecha, x + Math.cos(angulo) * xDistFlecha, y + Math.sin(angulo) * yDistFlecha, angulo, escala);
	}

	public void avanzar(Juego juego, Obstaculo[] obstaculos) {
		int widthPantalla = juego.getWidthPantalla();
		int heightPantalla = juego.getHeightPantalla();
		double widthColision, heightColision;
		double newX = x + Math.cos(angulo) * velocidad;
		double newY = y + Math.sin(angulo) * velocidad;
		if (transformada){
			widthColision = this.widthColisionTitan;
			heightColision = this.heightColisionTitan;
		}else{
			widthColision = this.widthColision;
			heightColision = this.heightColision;
		}
		Point newIzq = new Point(newX - widthColision, newY);
		Point newDer = new Point(newX + widthColision, newY);
		Point newArriba = new Point(newX, newY - heightColision);
		Point newAbajo = new Point(newX, newY + heightColision);
		Point newDiagonalArribaIzq = new Point(newX - widthColision, newY - heightColision);
		Point newDiagonalArribaDer = new Point(newX + widthColision, newY - heightColision);
		Point newDiagonalAbajoIzq = new Point(newX - widthColision, newY + heightColision);
		Point newDiagonalAbajoDer = new Point(newX + widthColision, newY + heightColision);
		boolean xColisiono = false;
		boolean yColisiono = false;
		if (newX <= widthColision || newX >= widthPantalla - widthColision){
			xColisiono = true;
			newX = x;
		}
		if (newY <= heightColision || newY >= heightPantalla - heightColision){
			yColisiono = true;
			newY = y;
		}
		for (Obstaculo obstaculo : obstaculos) {
			if (!xColisiono){
				xColisiono = obstaculo.colisionoObjetoEnX(x, newX, widthColision*0.8, newIzq, newDer, newDiagonalArribaIzq, newDiagonalArribaDer, newDiagonalAbajoIzq, newDiagonalAbajoDer);
			}
			if(!yColisiono){
				yColisiono = obstaculo.colisionoObjetoEnY(y, newY, heightColision*0.8, newArriba, newAbajo, newDiagonalArribaIzq, newDiagonalArribaDer, newDiagonalAbajoIzq, newDiagonalAbajoDer);
			}
		}
		if (!xColisiono) {
			izq.setX(newIzq.getX());
			der.setX(newDer.getX());
			arriba.setX(newArriba.getX());
			abajo.setX(newAbajo.getX());
			diagonalArribaIzq.setX(newDiagonalArribaIzq.getX());
			diagonalArribaDer.setX(newDiagonalArribaDer.getX());
			diagonalAbajoIzq.setX(newDiagonalAbajoIzq.getX());
			diagonalAbajoDer.setX(newDiagonalAbajoDer.getX());
			this.x = newX;
		}
		if (!yColisiono) {
			izq.setY(newIzq.getY());
			der.setY(newDer.getY());
			arriba.setY(newArriba.getY());
			abajo.setY(newAbajo.getY());
			diagonalArribaIzq.setY(newDiagonalArribaIzq.getY());
			diagonalArribaDer.setY(newDiagonalArribaDer.getY());
			diagonalAbajoIzq.setY(newDiagonalAbajoIzq.getY());
			diagonalAbajoDer.setY(newDiagonalAbajoDer.getY());
			this.y = newY;
		}
	}

	public void restarFlecha() {
		double newAngulo = angulo - Herramientas.radianes(velocidad * 0.7);
		angulo = (newAngulo <= 0) ? 2 * Math.PI : newAngulo;
		if (transformada){
			imgActual = (angulo < Math.PI * 1.5 && angulo > (double) Math.PI / 2) ? imgTitanIzq : imgTitanDer;
		}else{
			imgActual = (angulo < Math.PI * 1.5 && angulo > (double) Math.PI / 2) ? imgIzq : imgDer;
		}
	}

	public void sumarFlecha() {
		double newAngulo = angulo + Herramientas.radianes(velocidad * 0.7);
		angulo = (newAngulo >= 2 * Math.PI) ? 0 : newAngulo;
		if (transformada){
			imgActual = (angulo < Math.PI * 1.5 && angulo > (double) Math.PI / 2) ? imgTitanIzq : imgTitanDer;
		}else{
			imgActual = (angulo < Math.PI * 1.5 && angulo > (double) Math.PI / 2) ? imgIzq : imgDer;
		}
	}

	public Proyectil disparar(Juego juego, Obstaculo[] obstaculos) {
		int widthProyectil = Proyectil.getWidthProyectil();
		int heightProyectil = Proyectil.getHeightProyectil();
		double xProyectil = x + Math.cos(angulo);
		double yProyectil = y + Math.sin(angulo);
		double cosMitadWidth = Math.cos(angulo) * (widthProyectil / 2);
		double senoMitadHeight = Math.sin(angulo) * (widthProyectil / 2);
		Point frontLeft = new Point(
		xProyectil + (heightProyectil / 2) * Math.cos(angulo-Math.PI/2) + cosMitadWidth,
		yProyectil + (heightProyectil / 2) * Math.sin(angulo-Math.PI/2) + senoMitadHeight);
		Point frontRigth = new Point(
		xProyectil + (heightProyectil / 2) * Math.cos(angulo+Math.PI/2) + cosMitadWidth,
		yProyectil + (heightProyectil / 2) * Math.sin(angulo+Math.PI/2) + senoMitadHeight);
		for (Obstaculo obstaculo: obstaculos){
			if (
				Juego.puntoDentroDeRectangulo(frontLeft.getX(), frontLeft.getY(), obstaculo.getX(), obstaculo.getY(), obstaculo.getWidth(), obstaculo.getHeight()) ||
				Juego.puntoDentroDeRectangulo(frontRigth.getX(), frontRigth.getY(), obstaculo.getX(), obstaculo.getY(), obstaculo.getWidth(), obstaculo.getHeight())
			){ // si el proyectil se quiere crear sobre un obstaculo
				return null;
			}
		}
		return new Proyectil(xProyectil, yProyectil, angulo, juego, velocidad * 2);
	}

	public boolean tocoUnTitan(Titan[] titanes){
		for (int i = 0; i < titanes.length; i++) {
			if (titanes[i] != null && titanes[i].loToca(x, y, (int)widthColisionTitan, (int)heightColisionTitan)) {
				titanes[i] = null;
				return true;
			}
		}
		return false;
	}

	public void transformarEnTitan(){
		transformada = true;
		imgActual = (imgActual == imgDer) ? imgTitanDer : imgTitanIzq;
		xDistFlecha = widthTitan / 2 * 1.25;
		yDistFlecha = heightTitan / 2 * 1.25;
		izq = new Point(x - widthColisionTitan, y);
		der = new Point(x + widthColisionTitan, y);
		arriba = new Point(x, y - heightColisionTitan);
		abajo = new Point(x, y + heightColisionTitan);
		diagonalArribaIzq = new Point(x - widthColisionTitan, y - heightColisionTitan);
		diagonalArribaDer = new Point(x + widthColisionTitan, y - heightColisionTitan);
		diagonalAbajoIzq = new Point(x - widthColisionTitan, y + heightColisionTitan);
		diagonalAbajoDer = new Point(x + widthColisionTitan, y + heightColisionTitan);
	}

	public void volverFormaNormal(){
		transformada = false;
		imgActual = (imgActual == imgTitanDer) ? imgDer : imgIzq;
		xDistFlecha = width / 2 * 1.25;
		yDistFlecha = height / 2 * 1.25;
		izq = new Point(x - widthColision, y);
		der = new Point(x + widthColision, y);
		arriba = new Point(x, y - heightColision);
		abajo = new Point(x, y + heightColision);
		diagonalArribaIzq = new Point(x - widthColision, y - heightColision);
		diagonalArribaDer = new Point(x + widthColision, y - heightColision);
		diagonalAbajoIzq = new Point(x - widthColision, y + heightColision);
		diagonalAbajoDer = new Point(x + widthColision, y + heightColision);
	}

	public boolean seSuperpone(double x, double y, int width, int height){
		return (
		x <= this.x + this.width / 2 + width / 2 && x >= this.x - this.width / 2 - width / 2 &&
		y <= this.y + this.height / 2 + height / 2 && y >= this.y - this.height / 2 - height / 2)
		? true: false;
	}

	public boolean laToca(double x, double y, int width, int height){
		return (
		x <= this.x + this.widthColision + width && x >= this.x - this.widthColision - width &&
		y <= this.y + this.heightColision + height && y >= this.y - this.heightColision - height)
		? true : false;
	}

	public boolean estaCerca(double x, double y, int width, int height){
		return (
		x < this.x + this.width * 2 + width * 2 && x > this.x - this.width * 2 - width * 2 &&
		y < this.y + this.height * 2 + height * 2 && y > this.y - this.height * 2 - height * 2)
		? true: false;
	}

	// getters
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public double getAngulo(){
		return angulo;
	}
	public int getWidth() {
		return this.width;
	}
	public int getHeight() {
		return this.height;
	}
	public int getWidthTitan() {
		return this.widthTitan;
	}
	public int getHeightTitan() {
		return this.heightTitan;
	}
	public boolean estaTransformada(){
		return transformada;
	}
}