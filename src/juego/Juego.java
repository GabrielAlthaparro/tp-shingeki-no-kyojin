package juego;

import java.awt.Image;
import java.awt.Color;
import java.util.Random;
import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private final char JUGANDO = 0;
	private final char GANO = 1;
	private final char PERDIO = 2;
	private final char MENU = 3;
	private final char FACIL = 1;
	private final char NORMAL = 2;
	private final char DIFICIL = 3;
	private final char EXTREMO = 4;
	private int opcionMenu = NORMAL;
	private Image imgFondo;
	private int width = 800; // 1024
	private int height = 600;	// 650
	private Personaje mikasa;
	private Titan[] titanes;
	private int velocidadMikasa;
	private int velocidadTitan;
	private Proyectil proyectil;
	private Suero suero;
	private Obstaculo[] obstaculos;
	private int titanesVivos;
	private int titantesEliminados;
	private int estadoDelJuego = MENU;
	private Random random = new Random();
	private int randomJuego;
	private Clip sound;

	Juego(){
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Shingeki no Kyojin - Grupo 5 - v1", this.width, this.height);
		this.imgFondo = Herramientas.cargarImagen("fondo.jpeg");
		try {
			sound = Herramientas.cargarSonido("Ataek-on-Titan-corto-coro.aiff");
			sound.loop(-1);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		// Inicializar lo que haga falta para el juego
		// Inicia el juego!
		this.entorno.iniciar();
	}

	public void tick() {
		// Procesamiento de un instante de tiempo
		switch (estadoDelJuego) {
			case JUGANDO:
				this.entorno.dibujarImagen(imgFondo, width / 2, height / 2, 0);
				accionesObstaculos();
				accionesSuero();
				accionesMikasa();
				accionesProyectil();
				accionesTitanes();
				if (titanesVivos == 0) {
					estadoDelJuego = GANO;
				}
				entorno.cambiarFont("Algerian", 25, Color.black);
				entorno.escribirTexto("Titanes eliminados: " + titantesEliminados, 0, height);
				break;
			case MENU:
				menu();
				break;
			default :
				if (estadoDelJuego == GANO){
					entorno.cambiarFont("Arial", 25, Color.YELLOW);
					entorno.escribirTexto("Ganaste!", width / 2 - width * 0.07, height / 2 - height * 0.1);
					entorno.escribirTexto("Titanes eliminados: " + titantesEliminados, width / 2 - width * 0.16, height / 2);
				} else { // PERDIO
					entorno.cambiarFont("Arial", 25, Color.RED);
					entorno.escribirTexto("Game Over", width / 2 - width * 0.083, height / 2 - height * 0.05);
				}
				entorno.escribirTexto("Presione Enter", width / 2 - width * 0.1, height / 2 + height * 0.1);
				if (entorno.sePresiono(entorno.TECLA_ENTER) || entorno.sePresiono(entorno.TECLA_ESC)){
					estadoDelJuego = MENU;
				}
				break;
		}
	}

	private void menu(){
		if (entorno.sePresiono(entorno.TECLA_ESC)){
			System.exit(0);
		}
		entorno.cambiarFont("Arial", 30, Color.YELLOW);
		entorno.escribirTexto("Shingeki no Kyokin", width / 2 - width * 0.175, height * 0.15);
		entorno.cambiarFont("Arial", 25, Color.WHITE);
		entorno.escribirTexto("Dificultad:", width / 2 - width * 0.09, height * 0.3);
		entorno.cambiarFont("Arial", 25, Color.GREEN);
		entorno.escribirTexto("Fácil", width / 2 - width * 0.05, height / 2);
		entorno.cambiarFont("Arial", 25, Color.BLUE);
		entorno.escribirTexto("Normal", width / 2 - width * 0.065, height / 2 + height * 0.1);
		entorno.cambiarFont("Arial", 25, Color.ORANGE);
		entorno.escribirTexto("Difícil", width / 2 - width * 0.055, height / 2 + height * 0.2);
		entorno.cambiarFont("Arial", 25, Color.RED);
		entorno.escribirTexto("Extremo", width / 2 - width * 0.068, height / 2 + height * 0.3);
		if (entorno.sePresiono(entorno.TECLA_W)){
			if (opcionMenu != FACIL){
				opcionMenu--;
			}
		}
		if (entorno.sePresiono(entorno.TECLA_S)){
			if (opcionMenu != EXTREMO){
				opcionMenu++;
			}
		}
		switch (opcionMenu) {
			case FACIL:
				entorno.cambiarFont("Arial", 25, Color.GREEN);
				entorno.escribirTexto(">", width / 2 - width * 0.1, height / 2);
			break;
			case NORMAL:
				entorno.cambiarFont("Arial", 25, Color.BLUE);
				entorno.escribirTexto(">", width / 2 - width * 0.1, height / 2 + height * 0.1);
			break;
			case DIFICIL:
				entorno.cambiarFont("Arial", 25, Color.ORANGE);
				entorno.escribirTexto(">", width / 2 - width * 0.1, height / 2 + height * 0.2);
			break;
			case EXTREMO:
				entorno.cambiarFont("Arial", 25, Color.RED);
				entorno.escribirTexto(">", width / 2 - width * 0.1, height / 2 + height * 0.3);
			break;
		}
		if (entorno.sePresiono(entorno.TECLA_ENTER) || entorno.sePresiono(entorno.TECLA_ESPACIO)){
			inicializarJuego();
		}
	}

	private void inicializarJuego(){
		switch (opcionMenu) {
			case FACIL:
				velocidadMikasa = 3;
				velocidadTitan = 1;
				randomJuego = 200 + random.nextInt(100);
			break;
			case NORMAL:
				velocidadMikasa = 4;
				velocidadTitan = 2;
				randomJuego = 150 + random.nextInt(75);
			break;
			case DIFICIL:
				velocidadMikasa = 5;
				velocidadTitan = 3;
				randomJuego = 100 + random.nextInt(50);
			case EXTREMO:
				velocidadMikasa = 6;
				velocidadTitan = 4;
				randomJuego = 50 + random.nextInt(25);
			break;
		}
		mikasa = new Personaje(this.width, this.height, velocidadMikasa);
		suero = null;
		proyectil = null;
		titantesEliminados = 0;
		int cantidadObstaculos = 4 + random.nextInt(3); // 4 5 6
		obstaculos = new Obstaculo[cantidadObstaculos];
		for (int i = 0; i < obstaculos.length; i++) {
			obstaculos[i] = new Obstaculo(this, obstaculos, mikasa);
		}
		titanes = new Titan[6];
		int cantidadTitanes = 4 + random.nextInt(3); // 4 5 6
		for (int i = 0; i < cantidadTitanes; i++) {
			titanes[i] = new Titan(this, obstaculos, titanes, mikasa, velocidadTitan);
		}
		titanesVivos = cantidadTitanes;
		estadoDelJuego = JUGANDO;
	}

	private void accionesObstaculos(){
		for (Obstaculo obstaculo : obstaculos) {
			obstaculo.dibujarse(entorno);
		}
	}

	private void accionesSuero(){
		if (suero != null){
			suero.dibujarse(entorno);
			if (suero.tocandoMikasa(mikasa)){
				mikasa.transformarEnTitan();
				suero = null;
			}
		}else{
			if (random.nextInt(randomJuego) == 0) {
				if (!mikasa.estaTransformada()){
					suero = new Suero(this, obstaculos, mikasa);
				}
			}
		}
	}

	private void accionesMikasa() {
		if (mikasa.estaTransformada()) {
			if (mikasa.tocoUnTitan(titanes)) { // aca ya se borra el titan
				titanesVivos--;
				mikasa.volverFormaNormal();
			}
		}
		if (entorno.estaPresionada(entorno.TECLA_W)) {
			mikasa.avanzar(this, obstaculos);
		}
		if (entorno.estaPresionada(entorno.TECLA_A)) {
			mikasa.restarFlecha();
		}
		if (entorno.estaPresionada(entorno.TECLA_D)) {
			mikasa.sumarFlecha();
		}
		if (entorno.sePresiono(entorno.TECLA_ESPACIO)) {
			if (!mikasa.estaTransformada() && proyectil == null) {
				proyectil = mikasa.disparar(this, obstaculos);
			}
		}
		mikasa.dibujarse(entorno);
	}

	private void accionesProyectil() {
		if (proyectil != null) {
			proyectil.avanzar();
			if (proyectil.chocoConTitan(titanes)){
				titanesVivos--;
				titantesEliminados++;
				proyectil = null;
				return;
			}
			if (!proyectil.chocoConObstaculo(obstaculos) && !proyectil.chocoConPantalla(this)){
				proyectil.dibujarse(entorno);
			}else{
				proyectil = null;
			}
		}
	}

	private void accionesTitanes(){
		for (Titan titan : titanes) {
			if (titan != null) {
				titan.avanzar(this, obstaculos, titanes, mikasa);
				titan.dibujarse(entorno);
				if (titan.tocandoMikasa(mikasa)){
					estadoDelJuego = PERDIO;
				}
			}
		}
		if (random.nextInt(randomJuego) == 0) {
			crearTitanes();
		}
	}

	private void crearTitanes(){
		int i = 0;
		if (titanesVivos >= 3) { // si hay mas de 2 titanes vivos, creo uno mas
			while (i < titanes.length) {
				if (titanes[i] == null) {
					titanes[i] = new Titan(this, obstaculos, titanes, mikasa, velocidadTitan);
					titanesVivos++;
					break;
				}
				i++;
			}
		} else {
			while (titanesVivos < 3) { // si hay menos de 3, creo hasta 3 titanes
				if (titanes[i] == null) {
					titanes[i] = new Titan(this, obstaculos, titanes, mikasa, velocidadTitan);
					titanesVivos++;
				}
				i++;
			}
		}
	}

	// utilidades generales

	public boolean random(){
		return random.nextInt(2) == 0 ? true : false;
	}

	public static boolean puntoDentroDeRectangulo(double x, double y, double xRectangulo, double yRectangulo, int width, int height) {
		return (
		x > xRectangulo - width / 2 && x < xRectangulo + width / 2 &&
		y > yRectangulo - height / 2 && y < yRectangulo + height / 2)
		? true : false;
	}

	public static boolean tocaLadoIzquierdoDeRectangulo(Point dArribaDer, Point der, Point dAbajoDer, double xIzqRec, double yArribaRec, double yAbajoRec){
		return 	(dArribaDer.getX() >= xIzqRec || dAbajoDer.getX() >= xIzqRec || der.getX() >= xIzqRec) // si me paso mas a la izquierda de la parte derecha del rectangulo
						&& (
							yEntreHeigth(dArribaDer.getY(), yArribaRec, yAbajoRec) ||
							yEntreHeigth(der.getY(), yArribaRec, yAbajoRec) ||
							yEntreHeigth(dAbajoDer.getY(), yArribaRec, yAbajoRec)
						) ? true : false;
	}

	public static boolean tocaLadoDerechoDeRectangulo(Point dArribaIzq, Point izq, Point dAbajoIzq, double xDerRec, double yArribaRec, double yAbajoRec){
		return 	(dArribaIzq.getX() <= xDerRec || dAbajoIzq.getX() <= xDerRec || izq.getX() <= xDerRec) // si me paso mas a la derecha que la parte izquierda del rectangulo
						&& (
							yEntreHeigth(dArribaIzq.getY(), yArribaRec, yAbajoRec) ||
							yEntreHeigth(izq.getY(), yArribaRec, yAbajoRec) ||
							yEntreHeigth(dAbajoIzq.getY(), yArribaRec, yAbajoRec)
						) ? true : false;
	}

	public static boolean tocaAbajoDeRectangulo(Point dArribaIzq, Point arriba, Point dArribaDer, double yAbajoRec, double xIzqRec, double xDerRec){
		return 	(dArribaIzq.getY() <= yAbajoRec || dArribaDer.getY() <= yAbajoRec || arriba.getY() <= yAbajoRec) // si me paso mas arriba que la parte de abajo del rectangulo
						&& (
							xEntreWidth(dArribaIzq.getX(), xIzqRec, xDerRec) ||
							xEntreWidth(arriba.getX(), xIzqRec, xDerRec) ||
							xEntreWidth(dArribaDer.getX(), xIzqRec, xDerRec)
						) ? true : false;
	}

	public static boolean tocaArribaDeRectangulo(Point dAbajoIzq, Point abajo, Point dAbajoDer, double yArribaRec, double xIzqRec, double xDerRec){
		return 	(dAbajoIzq.getY() >= yArribaRec || dAbajoDer.getY() >= yArribaRec	|| abajo.getY() >= yArribaRec) // si me paso mas abajo que la parte de arriba del rectangulo
						&& (
							xEntreWidth(dAbajoIzq.getX(), xIzqRec, xDerRec) ||
							xEntreWidth(abajo.getX(), xIzqRec, xDerRec) ||
							xEntreWidth(dAbajoDer.getX(), xIzqRec, xDerRec)
						) ? true : false;
	}

	public static boolean xEntreWidth(double x, double izq, double der){
		return (x >= izq && x <= der) ? true : false;
	}
	public static boolean yEntreHeigth(double y, double arriba, double abajo){
		return (y >= arriba && y <= abajo) ? true : false;
	}

	// getters

	public int getWidthPantalla() {
		return width;
	}

	public int getHeightPantalla() {
		return height;
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) throws InterruptedException {
		Juego juego = new Juego();
	}
}
