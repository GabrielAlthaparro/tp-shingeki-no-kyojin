package juego;

import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Titan {
  private double x, y;
  private double angulo;
  private int width, height;
  private double widthColision, heightColision;
  private double velocidad;
  private int ladoQueSeMueveMas;
  private double escala = 0.7;
  private Point arriba, abajo, izq, der;
  private Point diagonalArribaIzq;
  private Point diagonalArribaDer;
  private Point diagonalAbajoIzq;
  private Point diagonalAbajoDer;
  private Image imgIzq;
  private Image imgDer;
  private Image imgActual;

  Titan(Juego juego, Obstaculo[] obstaculos, Titan[] titanes, Personaje mikasa, int velocidad){
    Random random = new Random();
    ladoQueSeMueveMas = random.nextInt(3) - 1; // -1 0 1
    ladoQueSeMueveMas = ladoQueSeMueveMas * (random.nextInt(3) + 1); // -3 -2 -1 0 1 2 3
    this.velocidad = 1 + velocidad * random.nextDouble();
		imgIzq = Herramientas.cargarImagen("titan-izquierda.png");
		imgDer = Herramientas.cargarImagen("titan-derecha.png");
		width = (int) (imgDer.getWidth(null) * escala);
		height = (int) (imgDer.getHeight(null) * escala);
    double[] coordenadas = randomXY(juego, mikasa, obstaculos, titanes);
    x = coordenadas[0];
    y = coordenadas[1];
    angulo = Math.atan2(mikasa.getY() - y, mikasa.getX() - x);
    imgActual = (Math.cos(angulo) < 0) ? imgIzq : imgDer;
    widthColision = width * 0.35;
		heightColision = height * 0.45;
    izq = new Point(x - widthColision, y);
		der = new Point(x + widthColision, y);
		arriba = new Point(x, y - heightColision);
		abajo = new Point(x, y + heightColision);
		diagonalArribaIzq = new Point(x - widthColision, y - heightColision);
		diagonalArribaDer = new Point(x + widthColision, y - heightColision);
		diagonalAbajoIzq = new Point(x - widthColision, y + heightColision);
		diagonalAbajoDer = new Point(x + widthColision, y + heightColision);
  }

  public void dibujarse(Entorno entorno) {
    entorno.dibujarImagen(imgActual, x, y, 0, escala);
  }

  public void avanzar(Juego juego, Obstaculo[] obstaculos, Titan[] titanes, Personaje mikasa){
    int widthPantalla = juego.getWidthPantalla();
    int heightPantalla = juego.getHeightPantalla();
    double deltaX = mikasa.getX() - x;
    double deltaY = mikasa.getY() - y;
    angulo = Math.atan2(deltaY, deltaX);
    angulo += (Math.PI / 8) * ladoQueSeMueveMas;
    if (mikasa.estaTransformada()) {
      angulo += (angulo < Math.PI) ? Math.PI : -Math.PI;
    }
    imgActual = (Math.cos(angulo) < 0) ? imgIzq : imgDer;
    double newX = x + Math.cos(angulo) * velocidad;
    double newY = y + Math.sin(angulo) * velocidad;
    Point newIzq = new Point(newX - widthColision, newY);
		Point newDer = new Point(newX + widthColision, newY);
		Point newArriba = new Point(newX, newY - heightColision);
		Point newAbajo = new Point(newX, newY + heightColision);
		Point newDiagonalArribaIzq = new Point(newX - widthColision, newY - heightColision);
		Point newDiagonalArribaDer = new Point(newX + widthColision, newY - heightColision);
		Point newDiagonalAbajoIzq = new Point(newX - widthColision, newY + heightColision);
		Point newDiagonalAbajoDer = new Point(newX + widthColision, newY + heightColision);
		boolean xColisiono = false;
		boolean yColisiono = false;
    if (newX <= widthColision || newX >= widthPantalla - widthColision){
			xColisiono = true;
			newX = x;
		}
		if (newY <= heightColision || newY >= heightPantalla - heightColision){
			yColisiono = true;
			newY = y;
		}
		for (Obstaculo obstaculo : obstaculos) {
			if (!xColisiono){
				xColisiono = obstaculo.colisionoObjetoEnX(x, newX, widthColision, newIzq, newDer, newDiagonalArribaIzq, newDiagonalArribaDer, newDiagonalAbajoIzq, newDiagonalAbajoDer);
			}
			if(!yColisiono){
				yColisiono = obstaculo.colisionoObjetoEnY(y, newY, heightColision, newArriba, newAbajo, newDiagonalArribaIzq, newDiagonalArribaDer, newDiagonalAbajoIzq, newDiagonalAbajoDer);
			}
		}
    for (Titan titan : titanes) {
      if (titan != null && titan != this){
        if (!xColisiono){
          xColisiono = titan.colisionoObjetoEnX(x, newX, widthColision, newIzq, newDer, newDiagonalArribaIzq, newDiagonalArribaDer, newDiagonalAbajoIzq, newDiagonalAbajoDer);
        }
        if (!yColisiono){
          yColisiono = titan.colisionoObjetoEnY(y, newY, heightColision, newArriba, newAbajo, newDiagonalArribaIzq, newDiagonalArribaDer, newDiagonalAbajoIzq, newDiagonalAbajoDer);
        }
      }
    }
    if (!xColisiono) {
			izq.setX(newIzq.getX());
			der.setX(newDer.getX());
			arriba.setX(newArriba.getX());
			abajo.setX(newAbajo.getX());
			diagonalArribaIzq.setX(newDiagonalArribaIzq.getX());
			diagonalArribaDer.setX(newDiagonalArribaDer.getX());
			diagonalAbajoIzq.setX(newDiagonalAbajoIzq.getX());
			diagonalAbajoDer.setX(newDiagonalAbajoDer.getX());
			this.x = newX;
		}
		if (!yColisiono) {
			izq.setY(newIzq.getY());
			der.setY(newDer.getY());
			arriba.setY(newArriba.getY());
			abajo.setY(newAbajo.getY());
			diagonalArribaIzq.setY(newDiagonalArribaIzq.getY());
			diagonalArribaDer.setY(newDiagonalArribaDer.getY());
			diagonalAbajoIzq.setY(newDiagonalAbajoIzq.getY());
			diagonalAbajoDer.setY(newDiagonalAbajoDer.getY());
			this.y = newY;
		}
  }

  public boolean tocandoMikasa(Personaje mikasa){
    return !mikasa.estaTransformada() && mikasa.laToca(x, y, (int)(widthColision), (int)(heightColision))
    ? true : false;
  }

  private double[] randomXY(Juego juego, Personaje mikasa, Obstaculo[] obstaculos, Titan[] titanes) {
    double[] coordenadas = new double[2];
    double x, y;
    Random random = new Random();
    int widthPantalla = juego.getWidthPantalla();
    int heightPantalla = juego.getHeightPantalla();
    int mitadWidth = width / 2;
    int mitadHeight = height / 2;
    x = random.nextInt(widthPantalla - width) + mitadWidth;
    y = random.nextInt(heightPantalla - height) + mitadHeight;
    while (mikasa.estaCerca(x, y, width, height) || Obstaculo.rectanguloSobreObstaculo(obstaculos, x, y, (int) (width * 1.5), (int) (height * 1.5)) || rectanguloSobreTitan(titanes, x, y, width/*si pongo mas rango hay probabilidad de entrar en un ciclo infinito*/, height)){
      x = random.nextInt(widthPantalla - width) + mitadWidth;
      y = random.nextInt(heightPantalla - height) + mitadHeight;
    }
    coordenadas[0] = x;
    coordenadas[1] = y;
    return coordenadas;
  }

  public boolean colisionoObjetoEnX(double x, double newX, double widthColision, Point newIzq, Point newDer, Point newDiagonalArribaIzq, Point newDiagonalArribaDer, Point newDiagonalAbajoIzq, Point newDiagonalAbajoDer){
    double xIzqTitan = this.x - this.width / 2;
    double xDerTitan = this.x + this.width / 2;
    double yArribaTitan = this.y - this.height / 2;
    double yAbajoTitan = this.y + this.height / 2;
    if (x <= newX) { // el objeto se quiere mover a la derecha
      if (x + widthColision <= xIzqTitan) {
        if (Juego.tocaLadoIzquierdoDeRectangulo(newDiagonalArribaDer, newDer, newDiagonalAbajoDer, xIzqTitan, yArribaTitan, yAbajoTitan)){
          return true;
        }
      }
    } else { // el objeto se quiere mover a la izquierda
      if (x - widthColision >= xDerTitan) {
        if (Juego.tocaLadoDerechoDeRectangulo(newDiagonalArribaIzq, newIzq, newDiagonalAbajoIzq, xDerTitan, yArribaTitan, yAbajoTitan)) {
          return true;
        }
      }
    }
    return false;
  }

  public boolean colisionoObjetoEnY(double y, double newY, double heightColision, Point newArriba, Point newAbajo, Point newDiagonalArribaIzq, Point newDiagonalArribaDer, Point newDiagonalAbajoIzq, Point newDiagonalAbajoDer){
    double xIzqTitan = this.x - this.width / 2;
    double xDerTitan = this.x + this.width / 2;
    double yArribaTitan = this.y - this.height / 2;
    double yAbajoTitan = this.y + this.height / 2;
    if (y <= newY) { // moviendo a abajo
      if (y + heightColision <= yArribaTitan) {
        if (Juego.tocaArribaDeRectangulo(newDiagonalAbajoIzq, newAbajo, newDiagonalAbajoDer, yArribaTitan, xIzqTitan, xDerTitan)) {
          return true;
        }
      }
    } else { // moviendo a arriba
      if (y - heightColision >= yAbajoTitan) {
        if (Juego.tocaAbajoDeRectangulo(newDiagonalArribaIzq, newArriba, newDiagonalArribaDer, yAbajoTitan, xIzqTitan, xDerTitan)) {
          return true;
        }
      }
    }
    return false;
  }
  public static boolean rectanguloSobreTitan(Titan[] titanes, double x, double y, int width, int height) {
    for (Titan titan : titanes) {
      if (titan != null) {
        if (seSuperponeEnY(titan, y, height)) {
          // me fijo ahora en x
          if (x < titan.getX()) { // si mi x esta a la izquierda del titan ya creado
            if (x + width >= titan.getX()) { // si sumandole el width a mi x se superpone
              return true;
            }
          } else { // si mi x esta a la derecha del titan ya creado
            if (x - width <= titan.getX()) { // si restandole el width a mi x se suporpone
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  private static boolean seSuperponeEnY(Titan titan, double y, int height){
    if (y < titan.getY()) { // si mi y esta arriba del titan ya creado
      if (y + height >= titan.getY()) { // si sumandole el height a mi y se superpone
        return true;
      }
    } else { // si estoy abajo
      if (y - height <= titan.getY()) { // si restandole el height a mi y se superpone
        return true;
      }
    }
    return false;
  }

  public boolean loToca(double x, double y, int width, int height){
		return (
		x <= this.x + this.widthColision + width && x >= this.x - this.widthColision - width &&
		y <= this.y + this.heightColision + height && y >= this.y - this.heightColision - height)
		? true : false;
	}

  // getters

	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
  public double getWidthColision(){
		return widthColision;
	}
	public double getHeightColision(){
		return heightColision;
	}
}
