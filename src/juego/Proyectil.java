package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Proyectil {
	private double x, y, angulo;
	private Point frontLeft, frontRigth;
	private static int widthProyectil;
	private static int heightProyectil;
	private double escala = 0.6;
	private int velocidad;
	private Image img;

	Proyectil(double x, double y, double angulo, Juego juego, int velocidad) {
		this.x = x;
		this.y = y;
		this.angulo = angulo;
		frontLeft = new Point(x + (heightProyectil / 2) * Math.cos(angulo-Math.PI/2) + Math.cos(angulo) * (widthProyectil / 2),
		y + (heightProyectil / 2) * Math.sin(angulo-Math.PI/2) + Math.sin(angulo) * (widthProyectil / 2));
		frontRigth = new Point(x + (heightProyectil / 2) * Math.cos(angulo+Math.PI/2) + Math.cos(angulo) * (widthProyectil / 2),
		y + (heightProyectil / 2) * Math.sin(angulo+Math.PI/2) + Math.sin(angulo) * (widthProyectil / 2));
		this.velocidad = velocidad;
		img = Herramientas.cargarImagen("proyectil.png");
		widthProyectil = (int) (img.getWidth(null) * escala);
		heightProyectil = (int) (img.getHeight(null) * escala);
	}

	public void dibujarse(Entorno entorno) {
		entorno.dibujarImagen(img, x, y, angulo, escala);
	}

	public void avanzar() {
		this.x = this.x + Math.cos(angulo) * velocidad;
		this.y = this.y + Math.sin(angulo) * velocidad;
		frontLeft.setX(x + (heightProyectil / 2) * Math.cos(angulo-Math.PI/2) + Math.cos(angulo) * (widthProyectil / 2));
		frontLeft.setY(y + (heightProyectil / 2) * Math.sin(angulo-Math.PI/2) + Math.sin(angulo) * (widthProyectil / 2));
		frontRigth.setX(x + (heightProyectil / 2) * Math.cos(angulo+Math.PI/2) + Math.cos(angulo) * (widthProyectil / 2));
		frontRigth.setY(y + (heightProyectil / 2) * Math.sin(angulo+Math.PI/2) + Math.sin(angulo) * (widthProyectil / 2));
	}

	public boolean chocoConTitan(Titan[] titanes){
		for (int i = 0; i < titanes.length; i++) {
			if (titanes[i] != null) {
				Titan titan = titanes[i];
				if(
					Juego.puntoDentroDeRectangulo(frontLeft.getX(), frontLeft.getY(), titan.getX(), titan.getY(), titan.getWidth(), titan.getHeight()) ||
					Juego.puntoDentroDeRectangulo(frontRigth.getX(), frontRigth.getY(), titan.getX(), titan.getY(), titan.getWidth(), titan.getHeight())
				){
					titanes[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	public boolean chocoConObstaculo(Obstaculo[] obstaculos){
		for (Obstaculo obstaculo: obstaculos){
			if(
				Juego.puntoDentroDeRectangulo(frontLeft.getX(), frontLeft.getY(), obstaculo.getX(), obstaculo.getY(), obstaculo.getWidth(), obstaculo.getHeight())||
				Juego.puntoDentroDeRectangulo(frontRigth.getX(), frontRigth.getY(), obstaculo.getX(), obstaculo.getY(), obstaculo.getWidth(), obstaculo.getHeight())
			){
				return true;
			}
		}
		return false;
	}

	public boolean chocoConPantalla(Juego juego){
		return(x > 0 && x < juego.getWidthPantalla() && y > 0 && y < juego.getHeightPantalla()) ? false : true;
	}

	//utlidades
	public static int getWidthProyectil(){
		return widthProyectil;
	}
	public static int getHeightProyectil(){
		return heightProyectil;
	}

	// getters
	public double getAngulo(){
		return angulo;
	}
}
