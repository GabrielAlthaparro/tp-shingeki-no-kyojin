package juego;

import java.util.Random;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {
  private double x, y;
  private int width, height;
  private Image img;

  Obstaculo(Juego juego, Obstaculo[] obstaculos, Personaje mikasa) {
    img = Herramientas.cargarImagen("casa.jpeg");
    width = img.getWidth(null);
		height = img.getHeight(null);
    double[] coordenadas = randomXY(juego, mikasa, obstaculos);
    x = coordenadas[0];
    y = coordenadas[1];
  }

  public void dibujarse(Entorno entorno) {
    entorno.dibujarImagen(img, x, y, 0);
  }

  private double[] randomXY(Juego juego, Personaje mikasa, Obstaculo[] obstaculos) {
    double[] coordenadas = new double[2];
    double x, y;
    int xRandom, yRandom;
    Random random = new Random();
    int widthPantalla = juego.getWidthPantalla();
    int heightPantalla = juego.getHeightPantalla();
    int mitadWidth = width / 2;
    int mitadheight = height / 2;
    int wMitadPantalla = widthPantalla / 2;
    int hMitadPantalla = heightPantalla / 2;

    xRandom = random.nextInt(wMitadPantalla + 1); // 0 a 800
    x = juego.random() ? mitadWidth + xRandom: wMitadPantalla + xRandom - mitadWidth;
    yRandom = random.nextInt(hMitadPantalla + 1); // 0 a 600
    y = juego.random() ? mitadheight + yRandom : hMitadPantalla + yRandom - mitadheight;
    while (mikasa.seSuperpone(x, y, width, height) ||
    rectanguloSobreObstaculo(obstaculos, x, y, width * 2/*multiplico por 2 para separar mas las casas*/ , height * 2)){
      xRandom = random.nextInt(wMitadPantalla + 1);
      x = juego.random() ? mitadWidth + xRandom: wMitadPantalla + xRandom - mitadWidth;
      yRandom = random.nextInt(hMitadPantalla + 1);
      y = juego.random() ? mitadheight + yRandom : hMitadPantalla + yRandom - mitadheight;
    }
    coordenadas[0] = x;
    coordenadas[1] = y;
    return coordenadas;
  }

  public static boolean rectanguloSobreObstaculo(Obstaculo[] obstaculos, double x, double y, int width, int height) {
    for (Obstaculo obstaculo : obstaculos) {
      if (obstaculo != null) {
        if (seSuperponeEnY(obstaculo, y, height)) {
          // me fijo ahora en x
          if (x < obstaculo.getX()) { // si mi x esta a la izquierda del obstaculo ya creado
            if (x + width >= obstaculo.getX()) { // si sumandole el width a mi x se superpone
              return true;
            }
          } else { // si mi x esta a la derecha del obstaculo ya creado
            if (x - width <= obstaculo.getX()) { // si restandole el width a mi x se suporpone
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  private static boolean seSuperponeEnY(Obstaculo obstaculo, double y, int height){
    if (y < obstaculo.getY()) { // si mi y esta arriba del obstaculo ya creado
      if (y + height >= obstaculo.getY()) { // si sumandole el height a mi y se superpone
        return true;
      }
    } else { // si estoy abajo
      if (y - height <= obstaculo.getY()) { // si restandole el height a mi y se superpone
        return true;
      }
    }
    return false;
  }

  public boolean colisionoObjetoEnX(double x, double newX, double widthColision, Point newIzq, Point newDer, Point newDiagonalArribaIzq, Point newDiagonalArribaDer, Point newDiagonalAbajoIzq, Point newDiagonalAbajoDer){
    double xIzqObs = this.x - this.width / 2;
    double xDerObs = this.x + this.width / 2;
    double yArribaObs = this.y - this.height / 2;
    double yAbajoObs = this.y + this.height / 2;
    if (x <= newX) { // moviendo a derecha
      if (x + widthColision <= xIzqObs) {
        if (Juego.tocaLadoIzquierdoDeRectangulo(newDiagonalArribaDer, newDer, newDiagonalAbajoDer, xIzqObs, yArribaObs, yAbajoObs)){
          return true;
        }
      }
    } else { // moviendo a izquierda
      if (x - widthColision >= xDerObs) {
        if (Juego.tocaLadoDerechoDeRectangulo(newDiagonalArribaIzq, newIzq, newDiagonalAbajoIzq, xDerObs, yArribaObs, yAbajoObs)) {
          return true;
        }
      }
    }
		return false;
	}

  public boolean colisionoObjetoEnY(double y,double newY, double heightColision, Point newArriba, Point newAbajo, Point newDiagonalArribaIzq, Point newDiagonalArribaDer, Point newDiagonalAbajoIzq, Point newDiagonalAbajoDer){
    double xIzqObs = this.x - this.width / 2;
    double xDerObs = this.x + this.width / 2;
    double yArribaObs = this.y - this.height / 2;
    double yAbajoObs = this.y + this.height / 2;
    if (y <= newY) { // moviendo a abajo
      if (y + heightColision <= yArribaObs) {
        if (Juego.tocaArribaDeRectangulo(newDiagonalAbajoIzq, newAbajo, newDiagonalAbajoDer, yArribaObs, xIzqObs, xDerObs)) {
          return true;
        }
      }
    } else { // moviendo a arriba
      if (y - heightColision >= yAbajoObs) {
        if (Juego.tocaAbajoDeRectangulo(newDiagonalArribaIzq, newArriba, newDiagonalArribaDer, yAbajoObs, xIzqObs, xDerObs)) {
          return true;
        }
      }
    }
		return false;
	}

	// getters

	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
}
